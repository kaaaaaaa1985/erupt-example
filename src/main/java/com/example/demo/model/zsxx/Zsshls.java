package com.example.demo.model.zsxx;

import com.example.demo.model.zsxx.enums.ZsxsShzt;
import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.model.base.HyperModel;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author yyu
 * @date 2021/7/26
 */
@Table(name = "t_zsgl_shls")
@Entity
@Erupt(name = "招生审核历史", power = @Power(add = false, edit = false, delete = false))
@Getter
@Setter
public class Zsshls extends HyperModel {

    @ManyToOne
    @EruptField(
            views = {@View(title = "姓名", column = "xm"), @View(title = "身份证号", column = "sfzh")}
    )
    private Zsxs zsxs;

    @EruptField(
            views = @View(title = "状态"),
            edit = @Edit(
                    notNull = true,
                    title = "状态",
                    type = EditType.CHOICE,
                    choiceType = @ChoiceType(fetchHandler = ZsxsShzt.ChoiceFetch.class)
            )
    )
    private Integer shzt;

    @EruptField(
            views = @View(title = "审核意见"),
            edit = @Edit(title = "审核意见", type = EditType.TEXTAREA)
    )
    private String shyj;


}
