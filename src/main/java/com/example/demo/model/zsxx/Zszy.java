package com.example.demo.model.zsxx;

import lombok.Getter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.handler.DictChoiceFetchHandler;
import xyz.erupt.upms.model.base.HyperModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "t_zsgl_zszy")
@Entity
@Erupt(name = "招生专业")
@Getter
public class Zszy extends HyperModel {

    @EruptField(
            views = @View(title = "年"),
            edit = @Edit(title = "年", type = EditType.DATE, dateType = @DateType(type = DateType.Type.YEAR), search = @Search)
    )
    private String nf;

    @EruptField(
            views = @View(title = "招生季节"),
            edit = @Edit(
                    search = @Search,
                    title = "招生季节", type = EditType.CHOICE, desc = "动态获取招生季节的值",
                    choiceType = @ChoiceType(
                            fetchHandler = DictChoiceFetchHandler.class,
                            fetchHandlerParams = "zsjj" //字典编码，通过字典编码获取字典项列表
                    ))
    )
    private Long zsjj;

    @EruptField(
            views = @View(title = "专业名称"),
            edit = @Edit(title = "专业名称", notNull = true, search = @Search)
    )
    private String zymc;

    @EruptField(
            views = @View(title = "是否使用"),
            edit = @Edit(title = "是否使用", notNull = true, search = @Search)
    )
    private Boolean sfsy;

    @EruptField(
            views = @View(title = "排序"),
            edit = @Edit(title = "排序")
    )
    private Integer px;


}
