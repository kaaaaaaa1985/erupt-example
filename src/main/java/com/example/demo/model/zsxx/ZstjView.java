package com.example.demo.model.zsxx;

import org.hibernate.annotations.Subselect;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.ReferenceTableType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.handler.DictChoiceFetchHandler;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @author yyu
 * @date 2021/7/27
 */
@Entity
@Erupt(name = "招生统计", power = @Power(add = false, edit = false, delete = false, viewDetails = false), primaryKeyCol = "zszy")
@Subselect("select zszy_id,xb,count(1) sl from t_zsgl_zsxs where shzt =2 group by zszy_id,xb ")
public class ZstjView implements Serializable {

    @Id
    @ManyToOne
    @EruptField(
            views = {@View(title = "年份", column = "nf"), @View(title = "专业", column = "zymc")},
            edit = @Edit(title = "专业", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType(label = "zymc"))
    )
    private Zszy zszy;

    @EruptField(
            views = @View(title = "性别"),
            edit = @Edit(
                    search = @Search,
                    title = "性别", type = EditType.CHOICE,
                    choiceType = @ChoiceType(
                            fetchHandler = DictChoiceFetchHandler.class,
                            fetchHandlerParams = "xb" //字典编码，通过字典编码获取字典项列表
                    ))
    )
    private String xb;

    @EruptField(
            views = @View(title = "数量")
    )
    private Integer sl;

}
