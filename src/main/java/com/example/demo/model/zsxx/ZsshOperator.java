package com.example.demo.model.zsxx;

import com.example.demo.model.zsxx.enums.ZsxsShzt;
import lombok.Getter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.jpa.model.BaseModel;

/**
 * @author yyu
 * @date 2021/7/26
 */
@Erupt(name = "Operator Dialog")
@Getter
public class ZsshOperator extends BaseModel {

    @EruptField(
            views = @View(title = "状态"),
            edit = @Edit(
                    notNull = true,
                    title = "状态",
                    type = EditType.CHOICE,
                    choiceType = @ChoiceType(fetchHandler = ZsxsShzt.ChoiceFetch.class)
            )
    )
    private Integer shzt;

    @EruptField(
            edit = @Edit(title = "审核意见", notNull = true, type = EditType.TEXTAREA)
    )
    private String shyj;


}
