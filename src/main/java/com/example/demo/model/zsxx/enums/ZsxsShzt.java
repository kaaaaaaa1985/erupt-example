package com.example.demo.model.zsxx.enums;

import lombok.Getter;
import xyz.erupt.annotation.fun.ChoiceFetchHandler;
import xyz.erupt.annotation.fun.VLModel;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author YuePeng
 * date 2021/4/19 10:48
 */
@Getter
public enum ZsxsShzt {
    WSH(1, "未审核"), SHTG(2, "审核通过"), SHBTG(3, "审核不通过");

    private final int value;
    private final String label;

    ZsxsShzt(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public static class ChoiceFetch implements ChoiceFetchHandler {

        @Override
        public List<VLModel> fetch(String[] params) {
            return Stream.of(ZsxsShzt.values())
                    .map(menuTypeEnum -> new VLModel(menuTypeEnum.getValue() + "", menuTypeEnum.getLabel()))
                    .collect(Collectors.toList());
        }

    }
}
