package com.example.demo.model.zsxx;

import com.example.demo.handler.zsgl.ZsxsHandlerImpl;
import com.example.demo.model.zsxx.enums.ZsxsShzt;
import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.annotation.sub_erupt.Drill;
import xyz.erupt.annotation.sub_erupt.Link;
import xyz.erupt.annotation.sub_erupt.RowOperation;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.Readonly;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.constant.RegexConst;
import xyz.erupt.upms.handler.DictChoiceFetchHandler;
import xyz.erupt.upms.model.base.HyperModel;

import javax.persistence.*;

@Table(name = "t_zsgl_zsxs", uniqueConstraints = {
        @UniqueConstraint(columnNames = "sfzh")
})
@Entity
@Erupt(name = "招生学生", dataProxy = Zsxs.class, rowOperation = {
        @RowOperation(
                operationHandler = ZsxsHandlerImpl.class,
                mode = RowOperation.Mode.MULTI,
                eruptClass = ZsshOperator.class,
                title = "批量审核")
}, drills = @Drill(
        title = "审核历史",
        link = @Link(
                linkErupt = Zsshls.class, joinColumn = "zsxs.id"
        )
))
@Getter
@Setter
public class Zsxs extends HyperModel implements DataProxy<Zsxs> {

    @EruptField(
            views = @View(title = "身份号码"),
            edit = @Edit(title = "身份号码", notNull = true, search = @Search(vague = true))
    )
    private String sfzh;

    @EruptField(
            views = @View(title = "性别"),
            edit = @Edit(
                    search = @Search,
                    title = "性别", type = EditType.CHOICE,
                    choiceType = @ChoiceType(
                            fetchHandler = DictChoiceFetchHandler.class,
                            fetchHandlerParams = "xb" //字典编码，通过字典编码获取字典项列表
                    ))
    )
    private String xb;

    @EruptField(
            views = @View(title = "姓名"),
            edit = @Edit(title = "姓名", notNull = true, search = @Search(vague = true))
    )
    private String xm;


    @ManyToOne
    @EruptField(
            views = {@View(title = "年份", column = "nf"), @View(title = "专业", column = "zymc")},
            edit = @Edit(title = "专业", type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType(label = "zymc"))
    )
    private Zszy zszy;

    @EruptField(
            views = @View(title = "状态"),
            edit = @Edit(
                    notNull = true,
                    readonly = @Readonly(add = false, edit = true),
                    title = "状态",
                    type = EditType.CHOICE,
                    choiceType = @ChoiceType(fetchHandler = ZsxsShzt.ChoiceFetch.class)
            )
    )
    private Integer shzt;

    @EruptField(
            views = @View(title = "手机号"),
            edit = @Edit(title = "手机号", notNull = true, search = @Search, inputType = @InputType(regex = RegexConst.PHONE_REGEX))
    )
    private String lxdh;

    @EruptField(
            views = @View(title = "家庭地址"),
            edit = @Edit(title = "家庭地址")
    )
    private String jtdz;

    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
    @Lob
    @EruptField(
            edit = @Edit(title = "备注", type = EditType.TEXTAREA)
    )
    private String bz;

    public void setShzt(Integer shzt) {
        this.shzt = shzt;
    }

    @Override
    public void addBehavior(Zsxs zsxs) {
        zsxs.setShzt(ZsxsShzt.WSH.getValue());
    }

    @Transient
    @EruptField(
            edit = @Edit(title = "年", show = false, type = EditType.DATE, dateType = @DateType(type = DateType.Type.YEAR), search = @Search)
    )
    private String zszy_nf;
}
