package com.example.demo.serivce;

import org.springframework.stereotype.Service;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.jpa.model.BaseModel;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * @author yyu
 * @date 2021/7/28
 */
@Service
public class DataService {

    @Resource
    private EruptDao eruptDao;

    @Transactional
    public void saveOrUpdate(BaseModel baseModel) {
        if (baseModel == null) {
            return;
        }
        if (baseModel.getId() == null) {
            eruptDao.persist(baseModel);
        } else {
            eruptDao.merge(baseModel);
        }
    }


}
