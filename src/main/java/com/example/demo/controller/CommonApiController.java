package com.example.demo.controller;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.erupt.annotation.fun.VLModel;
import xyz.erupt.core.constant.EruptRestPath;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptDictItem;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yyu
 * @date 2021/7/27
 */
@RestController
@RequestMapping(EruptRestPath.ERUPT_API)
public class CommonApiController {

    @Resource
    private EruptDao eruptDao;

    @GetMapping("/getDictItem")
    public List<VLModel> getDictItem(HttpServletRequest request) {
        String dictItem = request.getParameter("item");
        return
                eruptDao.queryEntityList(EruptDictItem.class, "eruptDict.code = '" + dictItem + "'", null)
                        .stream().map((item) -> new VLModel(item.getId(), item.getName())).collect(Collectors.toList());
    }

    @GetMapping("/getEntityItem")
    public List<VLModel> getEntityItem(HttpServletRequest request) {
        List<VLModel> list = new ArrayList<>();
        String className = request.getParameter("className");
        String displayColumn = request.getParameter("displayColumn");
        try {
            Class c3 = ClassUtils.getClass(className);
            List<Object> entityList = eruptDao.queryEntityList(c3);
            for (Object obj : entityList) {
                list.add(new VLModel(BeanUtils.getProperty(obj, "id"), BeanUtils.getProperty(obj, displayColumn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
