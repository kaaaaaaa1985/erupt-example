package com.example.demo.controller.api;

import com.example.demo.model.zsxx.Zsxs;
import com.example.demo.model.zsxx.enums.ZsxsShzt;
import com.example.demo.serivce.DataService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.erupt.core.constant.EruptRestPath;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yyu
 * @date 2021/7/28
 */
@RestController
@RequestMapping(EruptRestPath.ERUPT_API)
public class ZsfwApiController {

    @Resource
    private DataService dataService;

    @PostMapping({"/xsfw/save"})
    public Zsxs saveZsxs(@RequestBody Zsxs zsxs) {
        zsxs.setCreateTime(new Date());
        zsxs.setShzt(ZsxsShzt.WSH.getValue());
        dataService.saveOrUpdate(zsxs);
        return zsxs;
    }
}
