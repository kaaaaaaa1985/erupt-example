package com.example.demo.handler.zsgl;

import com.example.demo.model.ComplexOperator;
import com.example.demo.model.zsxx.ZsshOperator;
import com.example.demo.model.zsxx.Zsshls;
import com.example.demo.model.zsxx.Zsxs;
import org.springframework.stereotype.Component;
import xyz.erupt.annotation.fun.OperationHandler;
import xyz.erupt.jpa.dao.EruptDao;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author yyu
 * @date 2021/7/26
 */
@Component
public class ZsxsHandlerImpl implements OperationHandler<Zsxs, ZsshOperator> {

    @Resource
    private HttpServletRequest request; //展示自动注入功能

    @Resource
    private EruptDao eruptDao;

    @Override
    @Transactional
    public void exec(List<Zsxs> data, ZsshOperator operator, String[] param) {
        for (Zsxs zsxs : data) {
            zsxs.setShzt(operator.getShzt());
            eruptDao.merge(zsxs);
            Zsshls ls = new Zsshls();
            ls.setShzt(operator.getShzt());
            ls.setShyj(operator.getShyj());
            ls.setZsxs(zsxs);
            eruptDao.persist(ls);
        }
    }
}
