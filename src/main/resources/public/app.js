window.eruptSiteConfig = {
    domain: "",
    fileDomain: "",
    title: "Wisedu Framework",
    desc: "金智后台管理框架",
    dialogLogin: false,
    copyright: true, //是否保留显示版权信息
    logoPath: "wisedu.png",
    logoText: "Wisedu",
    registerPage: null,
    amapKey: null
};
